module.exports = function (router) {
    
    const project = require('./controller/project_ctrl');

    router.post('/project/addproject', project.addProject);
    router.post('/project/listproject', project.listProject);
    router.post('/project/deleteproject', project.deleteProject);
    router.get('/project/getprojectbyid', project.getProjectById);
    router.get('/project/updateProjectDetails', project.updateProjectDetails);

    return router;
}