'use strict';
var project = require('../models/project_model');
var query = require('../../lib/common_query');
var constant = require('../../lib/constant');
var Response = require('../../lib/response');
var moment = require('moment');


module.exports = {
    addProject: addProject,
    listProject: listProject,
    deleteProject: deleteProject,
    getProjectById: getProjectById,
    updateProjectDetails:updateProjectDetails
}

// Add Project  Details
function addProject(req, res) {
    async function addProjectExe() {
        try {
            if (!req.body.name) {
                return res.status(400).send({
                    message: "Please  Fill the required fields"
                });
            }
            const saveObj = {
                name: req.body.name,
                consultant: req.body.consultant,
                marketer: req.body.marketer,
                domain: req.body.domain,
                technology: req.body.technology,
                timezone: req.body.timezone,
                startdate: moment.utc(req.body.startdate).format()
            }
            const checkProjectNameExistObj = {
                name: req.body.name,
            }
            let checkprojectnameExist = await query.findoneData(project, checkProjectNameExistObj)
            if (checkprojectnameExist.data) {
                return res.json(Response(500, constant.validateMsg.projectexist));
            }
            else {
                let projectResult = await query.uniqueInsertIntoCollection(project, saveObj);
                if (projectResult.status) {
                    return res.json(Response(200, constant.messages.projectAdded));
                } else {
                    return res.json(Response(500, constant.validateMsg.internalError));
                }
            }
        } catch (error) {
            return res.json(Response(501, constant.validateMsg.internalError));
        }
    }
    addProjectExe().then(function (data) { });
}


//List project 
function listProject(req, res) {
    var count = req.body.count ? req.body.count : 0;
    var skip = req.body.count * (req.body.page - 1);
    var searchText = decodeURIComponent(req.body.searchText).replace(/[[\]{}()*+?,\\^$|#\s]/g, "\\s+");
    let obj = {
        deleted: false,
    }
    if (req.body.searchText) {
        obj.$or = [{
            'name': new RegExp(searchText, 'gi')
        }
        ];
    }
    async function listUserExe() {
        try {
            project.find(obj)
                .limit(parseInt(count))
                .skip(parseInt(skip))
                .lean()
                .sort({ 'createdAt': -1 })
                .exec(async function (err, result) {
                    if (err) {
                        return res.json(Response(500, constant.validateMsg.internalError, err));
                    } else {
                        let projectTotalCount = await query.countData(project, obj);
                        if (!projectTotalCount.status) {
                            res.json({ code: 500, message: constant.messages.commonError })
                        }
                        else {
                            res.json({ code: 200, data: result, totalCount: projectTotalCount.data, message: constant.messages.projectfetchSuccessfully })
                        }
                    }
                });
        } catch (error) {
            return res.json(Response(500, error.raw.message ? error.raw.message : constant.messages.commonError, error));

        }
    }
    listUserExe().then(function (data) { });
}
/// Delete Project

function deleteProject(req, res) {
    async function deleteProjectExe() {
        let cond = {
            _id: req.body.projectId
        }
        let upadteobj = {
            deleted: true
        }
        let deleteProjectResult = await query.updateOneDocument(project, cond, upadteobj)
        if (deleteProjectResult.status) {
            return res.json(Response(200, constant.messages.projectdeletedSucessfully, deleteProjectResult));
        } else {
            return res.json(Response(500, constant.validateMsg.internalError, err));
        }
    }
    deleteProjectExe().then(function (data) { });
}

/// Fetch data  project by Id 
function getProjectById(req, res) {
    async function getProjectByIdExe() {
        try {
            project.findOne({ _id: req.params.projectId })
                .lean()
                .exec(function (err, result) {
                    if (err) {
                        return res.json(Response(500, constant.validateMsg.internalError, err));
                    } else {
                        return res.json(Response(200, constant.messages.projectfetchSuccessfully, result));
                    }
                });
        } catch (error) {
            return res.json(Response(500, constant.validateMsg.internalError));
        }
    }
    getProjectByIdExe().then(function (data) { });
}

//update project details
function updateProjectDetails(req, res) {
    async function updateProjectDetailsExe() {
        try {
            if (!req.body) {
                return res.status(400).send({
                    message: "Please  Fill the required fields"
                });
            }
            let projectObj = {
                name: req.body.name,
                consultant: req.body.consultant,
                marketer: req.body.marketer,
                domain: req.body.domain,
                technology: req.body.technology,
                timezone: req.body.timezone,
                startdate: moment.utc(req.body.startdate).format()
            }
            let updateObjCon = {
                _id : req.body.projectId
            }
                let result = await query.updateOneDocument(project ,updateObjCon ,projectObj );
                if (result.status) {
                    return res.json(Response(200, constant.messages.projectupdatedSucessfully));
                } else {
                    return res.json(Response(500, constant.validateMsg.internalError));
                }
         
        } catch (error) {
            return res.json(Response(500, error.raw.message ? error.raw.message : constant.messages.commonError,error));

        }
    }
    updateProjectDetailsExe().then(function (data) { });
}