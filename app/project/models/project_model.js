'use strict';

var mongoose = require('mongoose');

var projectSchema= new mongoose.Schema({
    name 		     : { type: String},
    consultant		 : { type: String},
    marketer		 : { type: String}, 
    domain           : {type:String},
    technology       : {type:String},
    timezone         : { type: String},
    startdate        : { type: Date},
    deleted          : { type: Boolean, default: false },
},{
timestamps: true
});

module.exports = mongoose.model('project', projectSchema);