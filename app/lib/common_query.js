var commonQuery = {};

commonQuery.uniqueInsertIntoCollection = function uniqueInsertIntoCollection(model, obj) {
    let result = { status: false }
    return new Promise(function (resolve, reject) {
        new model(obj).save(function (err, userData) {
            if (err) {
                result.err = err
                reject(result);
            } else {
                result.status = true
                result.userData = userData
                resolve(result);
            }
        });
    })
}
commonQuery.findoneData = function findoneData(model, cond, fetchVal) {
    return new Promise(function (resolve, reject) {
        model.findOne(cond, fetchVal, function (err, userData) {
             console.log("userData", userData)
            let tempObj = {
                status: false
            }
            if (err) {
                tempObj.error = err;
                reject(tempObj);
            } else {
                tempObj.status = true;
                tempObj.data = userData;
                resolve(tempObj);
            }

        });
    })

}

commonQuery.countData = function countData(model, cond) {
    let tempObj = { status: false }
    return new Promise(function (resolve, reject) {
        model.count(cond).exec(function (err, userData) {
            if (err) {
                tempObj.error = err;
                reject(tempObj);
            } else {
                tempObj.status = true;
                tempObj.data = userData;
                resolve(tempObj);
            }

        });
    })
}

commonQuery.updateOneDocument = function updateOneDocument(model, updateCond, userUpdateData) {
    return new Promise(function (resolve, reject) {
        // let obj = { status: false };
        model.findOneAndUpdate(updateCond, {
            $set: userUpdateData
        }, {
                new: true
            })
            .lean().exec(function (err, userInfoData) {
                let tempObj = {
                    status: false
                }
                if (err) {
                    tempObj.error = err;
                    reject(tempObj);
                } else {
                    tempObj.status = true;
                    tempObj.data = userInfoData;
                    resolve(tempObj);
                }
            });
    })
}
module.exports = commonQuery;
