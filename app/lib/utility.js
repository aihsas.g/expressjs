
var constant = require('./constant');
var utility = {};

var crypto = require('crypto'),
    algorithm = constant.cryptoConfig.cryptoAlgorithm,
    password = constant.cryptoConfig.cryptoPassword;
utility.getEncryptText = function (text) {
    var cipher = crypto.createCipher(algorithm, password);
    text = cipher.update(text, 'utf8', 'hex');
    text += cipher.final('hex');
    return text;
}

module.exports = utility;
