
const statusCode = {
    "ok": 200,
    "error": 401,
    "warning": 404,
    "failed": 1002,
    "invalidURL": 1001,
    "unauth": 402,
    "internalError": 1004
}
const messages = {
    "userAdded": "User Added Successfully",
    "loginSuccess" :"Login Success",
    "requestNotProcessed" :"Something went wrong",
    "userfetchSuccessfully" :"User fetch Successfully",
    "userdeletedSucessfully" :"User Deleted Sucessfully",
    "userupdatedSucessfully" :"User Updated Sucessfully",
    "projectAdded": "Project Added Successfully",
    "projectfetchSuccessfully" :"Project fetch Successfully",
    "projectdeletedSucessfully" :"Project Deleted Sucessfully",
    "projectfetchSuccessfully" :"Project fetch Successfully",
    "projectupdatedSucessfully" :"Project Updated Sucessfully",


}

const validateMsg = {
    "internalError": "Internal error",
    "userID" :"User with Same User ID Already Exist",
    "userEmail" :"User with Same User Email Already Exist",
    "invalidUser": "Invalid credentials Please check Ecmployee Id or Password",
    "projectexist" :"Project with same Project Name Already Exist",



}
const cryptoConfig = {
    "cryptoAlgorithm": "aes-256-ctr",
    "cryptoPassword": 'd6F3Efeq',
    "secret": "Management",

}

const obj = {
    cryptoConfig : cryptoConfig,
    statusCode: statusCode,
    messages: messages,
    validateMsg: validateMsg,
};
module.exports = obj;