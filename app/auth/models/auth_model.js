'use strict';

var mongoose = require('mongoose');

var userSchema= new mongoose.Schema({
    username			 : { type: String},
    email		         : { type: String}, 
    employeeId           : {type:Number},
    password             : {type:String},
    token: { type: String, default: '' },
    deleted: { type: Boolean, default: false },

}, {
    timestamps: true
});

module.exports = mongoose.model('user', userSchema);