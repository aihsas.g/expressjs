'use strict';

var mongoose = require('mongoose');

var TokenSchema = new mongoose.Schema({
    token: { type: String },
    user_id : {type: String}
}, {
        timestamps: true
    });
var Token = mongoose.model('token', TokenSchema);
module.exports = Token;