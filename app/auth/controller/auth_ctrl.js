'use strict';
var user = require('../models/auth_model');
var query = require('../../lib/common_query');
var constant = require('../../lib/constant');
var utility = require('../../lib/utility');
var Response = require('../../lib/response');
var jwt = require('jsonwebtoken');
var Token = require('../models/token_model');

module.exports = {
    registerUser: registerUser,
    userLogin: userLogin,
    listUser: listUser,
    deleteUser: deleteUser,
    getUserById: getUserById,
    updateUser:updateUser
}

// User Registration 
function registerUser(req, res) {
    async function registerUserExe() {
        try {
            if (!req.body.email && !req.body.employeeId) {
                return res.status(400).send({
                    message: "Please  Fill the required fields"
                });
            }
            const saveObj = {
                username: req.body.username,
                email: req.body.email,
                employeeId: req.body.employeeId,
                password: req.body.password ? utility.getEncryptText(req.body.password) : '',

            }
            const checkUserNameExistObj = {
                username: req.body.username,
            }
            const checkUseremailExistObj = {
                email: req.body.email,
            }
            let checkusernameExist = await query.findoneData(user, checkUserNameExistObj)
            let checkuseremailExist = await query.findoneData(user, checkUseremailExistObj)
            if (checkusernameExist.data) {
                return res.json(Response(500, constant.validateMsg.userID));
            }
            else if (checkuseremailExist.data) {
                return res.json(Response(500, constant.validateMsg.userEmail));

            }
            else {
                let result = await query.uniqueInsertIntoCollection(user, saveObj);
                if (result.status) {
                    return res.json(Response(200, constant.messages.userAdded));
                } else {
                    return res.json(Response(500, constant.validateMsg.internalError));
                }
            }
        } catch (error) {
            console.log("signup error", error)
            return res.json(Response(501, constant.validateMsg.internalError));
        }
    }
    registerUserExe().then(function (data) { });
}

//User Login

function userLogin(req, res) {
    console.log("req.body", req.body)
    async function userLoginExe() {
        try {
            if (!req.body.employeeId && !req.body.password) {
                return res.status(400).send({
                    message: "Please  Fill the required fields"
                });
            }
            var jwtToken = null;
            var passEnc = utility.getEncryptText(req.body.password);
            var userData = {
                password: passEnc, //req.body.password
                employeeId: req.body.employeeId
            };
            console.log("user Login", userData)
            user.findOne(userData, {
                password: 0,
                updatedAt: 0,
                createdAt: 0,
                email: 0

            }).exec(function (err, userInfo) {
                if (userInfo) {
                    console.log("userInfo", userInfo)
                    var expirationDuration = 60 * 60 * 24 * 15; // expiration duration format sec:min:hour:day. ie: 8 Hours 
                    var params = {
                        uid: userInfo._id
                    }
                    jwtToken = jwt.sign(params, constant.cryptoConfig.secret, {
                        expiresIn: expirationDuration
                    });
                    userInfo.token = jwtToken;
                    userInfo.save(function (err, userInfoData) {
                        if (err) {
                            res.jsonp(Error(constant.statusCode.error, constant.messages.requestNotProcessed, err));
                        } else {
                            new Token({ token: jwtToken, user_id: userInfoData._id }).save(function (err, result) {
                                if (err) {
                                    res.jsonp(Error(constant.statusCode.error, constant.messages.requestNotProcessed, err));
                                } else {
                                    res.json(Response(constant.statusCode.ok, constant.messages.loginSuccess, userInfoData, null));
                                }

                            });
                        }
                    });
                }
                else {
                    console.log("err", err)
                    return res.json(Response(404, constant.validateMsg.invalidUser));
                }
            })

        } catch (error) {
            console.log("catch error", error)
            return res.json(Response(501, constant.validateMsg.internalError));
        }
    }
    userLoginExe().then(function (data) { });
}

//list user  with pagination and  search

function listUser(req, res) {
    var count = req.body.count ? req.body.count : 0;
    var skip = req.body.count * (req.body.page - 1);
    var searchText = decodeURIComponent(req.body.searchText).replace(/[[\]{}()*+?,\\^$|#\s]/g, "\\s+");
    let obj = {
        deleted: false,
    }
    if (req.body.searchText) {
        obj.$or = [{
            'email': new RegExp(searchText, 'gi')
        }
        ];
    }
    async function listUserExe() {
        try {
            user.find(obj)
                .limit(parseInt(count))
                .skip(parseInt(skip))
                .lean()
                .sort({ 'createdAt': -1 })
                .exec(async function (err, result) {
                    if (err) {
                        console.log("whats if error")
                        return res.json(Response(500, constant.validateMsg.internalError, err));
                    } else {
                        let userTotalCount = await query.countData(user, obj);
                        if (!userTotalCount.status) {
                            res.json({ code: 500, message: constant.messages.commonError })
                        }
                        else {
                            res.json({ code: 200, data: result, totalCount: userTotalCount.data, message: constant.messages.userfetchSuccessfully })
                        }
                    }
                });
        } catch (error) {
            console.log("whats catch eror", error)
            return res.json(Response(500, error.raw.message ? error.raw.message : constant.messages.commonError, error));

        }
    }
    listUserExe().then(function (data) { });
}
/// Delete user Api

function deleteUser(req, res) {
    async function deleteUserExe() {
        let cond = {
            _id: req.body.userId
        }
        let upadteobj = {
            deleted: true
        }
        let deleteUserResult = await query.updateOneDocument(user, cond, upadteobj)
        if (deleteUserResult.status) {
            return res.json(Response(200, constant.messages.userdeletedSucessfully, deleteUserResult));
        } else {
            return res.json(Response(500, constant.validateMsg.internalError, err));
        }
    }
    deleteUserExe().then(function (data) { });
}

/// Get user by Id User Api
function getUserById(req, res) {
    async function getUserByIdExe() {
        try {
            user.findOne({ _id: req.params.userId })
                .lean()
                .exec(function (err, result) {
                    if (err) {
                        return res.json(Response(500, constant.validateMsg.internalError, err));
                    } else {
                        return res.json(Response(200, constant.messages.userfetchSuccessfully, result));
                    }
                });
        } catch (error) {
            return res.json(Response(500, constant.validateMsg.internalError));
        }
    }
    getUserByIdExe().then(function (data) { });
}




function updateUser(req, res) {
    async function updateUserExe() {
        try {
            if (!req.body) {
                return res.status(400).send({
                    message: "Please  Fill the required fields"
                });
            }
            let userObj = {
                username: req.body.username,
                email: req.body.email,
            }
            let updateObjCon = {
                _id : req.body.userId
            }
                let result = await query.updateOneDocument(user ,updateObjCon ,userObj );
                if (result.status) {
                    return res.json(Response(200, constant.messages.userupdatedSucessfully));
                } else {
                    return res.json(Response(500, constant.validateMsg.internalError));
                }
         
        } catch (error) {
            return res.json(Response(500, error.raw.message ? error.raw.message : constant.messages.commonError,error));

        }
    }
    updateUserExe().then(function (data) { });
}