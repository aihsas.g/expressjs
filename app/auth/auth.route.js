module.exports = function (router) {
    
    const auth = require('./controller/auth_ctrl');

    router.post('/user/registerUser', auth.registerUser);
    router.post('/user/userLogin', auth.userLogin);
    router.post('/user/listUser', auth.listUser);
    router.post('/user/deleteUser', auth.deleteUser);
    router.get('/user/getUserById', auth.getUserById);
    router.post('/user/updateUser', auth.updateUser);

    return router;
}

