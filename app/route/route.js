module.exports = function (express) {
    const router = express.Router()
    require('../auth/auth.route')(router);
    require('../project/project.route')(router);


    return router;

}